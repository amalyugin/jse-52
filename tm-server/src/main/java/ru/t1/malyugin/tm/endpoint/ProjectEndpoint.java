package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.dto.IProjectDTOService;
import ru.t1.malyugin.tm.api.service.model.IProjectService;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.dto.request.project.*;
import ru.t1.malyugin.tm.dto.response.project.*;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.server.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    private IProjectDTOService getProjectDTOService() {
        return getServiceLocator().getProjectDTOService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        try {
            getProjectDTOService().changeStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            getProjectDTOService().changeStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            getProjectDTOService().changeStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            getProjectDTOService().updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectUpdateByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            getProjectService().removeById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse creteProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectDTOService().create(userId, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        try {
            getProjectService().clear(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectDTOService().findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowListResponse showProjectList(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowListRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<ProjectDTO> projectList;
        try {
            projectList = getProjectDTOService().findAll(userId, sort);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectShowListResponse(projectList);
    }

}