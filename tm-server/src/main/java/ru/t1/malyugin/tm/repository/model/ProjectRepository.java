package ru.t1.malyugin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.model.IProjectRepository;
import ru.t1.malyugin.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractWBSRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Class<Project> getClazz() {
        return Project.class;
    }

}