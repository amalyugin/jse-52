package ru.t1.malyugin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerToMongoService implements ILoggerService {

    @NotNull
    private final static String MONGO_HOST_KEY = "MONGO_HOST";

    @NotNull
    private final static String MONGO_PORT_KEY = "MONGO_PORT";

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoDatabase mongoDatabase;

    public LoggerToMongoService() {
        @NotNull String host = "localhost";
        @NotNull String port = "27017";

        if (System.getenv().containsKey(MONGO_HOST_KEY)) host = System.getenv(MONGO_HOST_KEY);
        if (System.getenv().containsKey(MONGO_PORT_KEY)) port = System.getenv(MONGO_PORT_KEY);

        @NotNull MongoClient mongoClient = new MongoClient(host, Integer.parseInt(port));
        mongoDatabase = mongoClient.getDatabase("TM_LOG");
    }

    @Override
    @SneakyThrows
    public void log(@NotNull final String text) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}