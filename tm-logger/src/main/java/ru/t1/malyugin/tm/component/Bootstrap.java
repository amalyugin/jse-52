package ru.t1.malyugin.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.ILoggerService;
import ru.t1.malyugin.tm.listener.EntityListener;
import ru.t1.malyugin.tm.service.LoggerToMongoService;

import javax.jms.*;

@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final static String QUEUE_URL_KEY = "QUEUE_URL";

    @NotNull
    private final static String QUEUE_NAME_KEY = "QUEUE_NAME";

    @SneakyThrows
    public void run(@Nullable final String... args) {
        BasicConfigurator.configure();

        @NotNull String url = ActiveMQConnection.DEFAULT_BROKER_URL;
        @NotNull String queue = "TM_LOGGER";

        if (System.getenv().containsKey(QUEUE_URL_KEY)) url = System.getenv(QUEUE_URL_KEY);
        if (System.getenv().containsKey(QUEUE_NAME_KEY)) queue = System.getenv(QUEUE_NAME_KEY);

        @NotNull final ILoggerService loggerService = new LoggerToMongoService();
        @NotNull final EntityListener entityListener = new EntityListener(loggerService);
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(queue);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}